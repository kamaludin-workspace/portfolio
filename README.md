# Portfolio

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/kamaludin-workspace/portfolio/badge)](https://www.codefactor.io/repository/bitbucket/kamaludin-workspace/portfolio)

Front-end app for web portfolio [https://www.kamaludin.tech/](https://kamaludin.tech/)

## Tech Stack

- Vue.JS
- Tailwindcss
- Vercel

## Task

- [✔] Blog layout
- [❌] API Blog
- [❌] Glass effect at mobile menu
- [✔] Loading when submit form
- [✔] Fixed html tag basic seo