import Vue from 'vue'
import App from './App.vue'
import router from './router'
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";
import './assets/tailwind.css'
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.$http = axios

Sentry.init({
  Vue,
  dsn: "https://2d5cbc684bbd4459bd21618e0c8eccc2@o988196.ingest.sentry.io/5945347",
  integrations: [
    new Integrations.BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
      tracingOrigins: ["localhost", "kamaludin.tech", /^\//],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')