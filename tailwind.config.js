const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    mode: 'layers',
    content: [
      './public/**/*.html',
      './src/**/*.vue'
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors:  {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      blue: colors.lightBlue,
      gray: colors.coolGray,
      green: colors.green,
      cyan: colors.cyan,
      yellow: colors.yellow,
      orange: colors.orange,
      fuchsia: colors.fuchsia,
      purple: colors.purple,
      rose: colors.rose
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
